from flask_pymongo import PyMongo
from flask import Flask, render_template
from werkzeug.wrappers import request
from flask import request
app = Flask(__name__)
app.config["MONGO_URI"] = "mongodb://localhost:27017/blogsDatabase"
mongo = PyMongo(app)

@app.route("/")
def home():
    return render_template('index.html')


@app.route("/about")
def about():
    return render_template('about.html')

@app.route("/post")
def post():
    return render_template('post.html')
# @app.route("/contact")
# def contact():
#     return render_template('contact.html')

@app.route("/contact", methods=['GET' , 'POST'])
def contact():
    if request.method == "POST" :
        data={}
        data['name']=request.form.get['name']
        data['email']=request.form.get["email"]
        data['phone']=request.form.get["phone"]
        data['message']=request.form.get["message"]
        mongo.db.blogsDB1.insert_one(data)  
        # print()
        print("data entered")
        # '''logic of extrating the data and dump it into database'''
    return render_template('contact.html')


app.run(host='127.0.2.1',debug=True,port=5000)

